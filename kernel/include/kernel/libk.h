#ifndef __KERNEL_LIBK
#define __KERNEL_LIBK 1

#include <stddef.h>
#include <stdint.h>

size_t kstrlen(const char*);
int kprintf(const char* __restrict, ...);
int kputchar(int);
int kputs(const char*);

#define MYNAME "KERNEL"
static const char myname[]=MYNAME;

#ifdef DO_DEBUG
	#define DEBUG(fmt,args...) kprintf("DEBUG: %s %s %s %d: " fmt "\n",myname,__BASE_FILE__,__FUNCTION__,__LINE__,## args)
#else // DO_DEBUG
	#define DEBUG(fmt,args...)
#endif // DO_DEBUG

#define BUG()

// warnings are always show as KERN_WARNING; WARNING does not do a BUG() (guaranteed)
#define WARNING(fmt,args...) kprintf("WARNING: %s %s %s %d: " fmt "\n",myname,__BASE_FILE__,__FUNCTION__,__LINE__,## args);

// errors are always shown as KERN_ERR; ERROR does not do a BUG()
#define ERROR(fmt,args...) do { kprintf("ERROR: %s %s %s %d: " fmt "\n",myname,__BASE_FILE__,__FUNCTION__,__LINE__,## args); BUG(); } while(0)

// fatals are always shown as KERN_ERR; FATAL also does a BUG()
#define FATAL(fmt,args...) do { kprintf("FATAL: %s %s %s %d: " fmt "\n",myname,__BASE_FILE__,__FUNCTION__,__LINE__,## args); BUG(); } while(0)

// prints are always show as KERN_INFO 
#define PRINT(fmt,args...) kprintf("PRINT: %s %s %s %d: " fmt "\n",myname,__BASE_FILE__,__FUNCTION__,__LINE__,## args);

#endif