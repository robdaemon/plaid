#include <stddef.h>
#include <stdint.h>

#include <kernel/tty.h>
#include <kernel/libk.h>

extern "C" void kernel_early(void) {
	terminal_initialize();
}

extern "C" void kernel_main(void) {
	kprintf("Hello, kernel World!\n\n\nNewlines work now.\tSo do tabs.\n\tAnd on a new line.");
}
